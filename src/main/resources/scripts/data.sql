INSERT INTO subreddits (sub_red_title)
VALUES ('Music');
INSERT INTO subreddits (sub_red_title)
VALUES ('Novel');
INSERT INTO subreddits (sub_red_title)
VALUES ('Spring');

INSERT INTO posts (post_title, post_description,  created_date, vote, subreddit_id)
VALUES ('Guitar', 'First Guitar Lesson',  now(), 0, 1);
INSERT INTO posts (post_title, post_description,  created_date, vote, subreddit_id)
VALUES ('THe Hobbit', 'Adventure of tiny hobbit',   now(), 0, 2);
INSERT INTO posts (post_title, post_description,  created_date, vote, subreddit_id)
VALUES ('SpringBoot', 'First Spring application',  now(), 0, 3);

INSERT INTO comments (description, created_date, vote, post_id)
VALUES ('hi', now(), 0, 1);
INSERT INTO comments (description, created_date, vote, post_id)
VALUES ('hello', now(), 0, 1);
INSERT INTO comments (description, created_date, vote, post_id)
VALUES ('how doing', now(), 0, 2);
INSERT INTO comments (description, created_date, vote, post_id)
VALUES ('my comment', now(), 0, 3);
