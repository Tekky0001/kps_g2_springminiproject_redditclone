drop table if exists posts CASCADE ;
drop table if exists comments CASCADE ;
drop table if exists subreddits;

create table if not exists subreddits(
     id serial primary key not null,
     sub_red_title varchar(255) not null
);
create table if not exists posts(
    id serial primary key not null,
    post_title varchar(255) not null,
    post_description varchar(255) not null,
    image_url varchar(255),
    created_date timestamp,
    vote int4 not null,
    subreddit_id int4 not null references subreddits(id) ON DELETE CASCADE
);

create table if not exists comments(
    id serial primary key not null,
    description varchar(255),
    created_date timestamp,
    vote int4 not null,
    post_id int4 not null references posts(id) ON DELETE CASCADE
)