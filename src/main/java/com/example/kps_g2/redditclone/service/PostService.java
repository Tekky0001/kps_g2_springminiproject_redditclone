package com.example.kps_g2.redditclone.service;

import com.example.kps_g2.redditclone.model.Post;

import java.util.List;

public interface PostService {

    List<Post> getAllPosts();
    Post findPostById(Integer id);

    void updatePost(Post post);

    List<Post> getAllPostsBySubRedditId(int id);

    void insertPost(Post post);

    void deletePostById(Integer id);
}
