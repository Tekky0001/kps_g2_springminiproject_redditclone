package com.example.kps_g2.redditclone.service;

import com.example.kps_g2.redditclone.model.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> getAllComments();

    void addComment(Comment comment);

    Comment findCommentById(Integer id);

    void updateComment(Comment comment);
}
