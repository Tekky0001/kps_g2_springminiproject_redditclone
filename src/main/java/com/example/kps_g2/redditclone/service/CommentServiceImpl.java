package com.example.kps_g2.redditclone.service;

import com.example.kps_g2.redditclone.model.Comment;
import com.example.kps_g2.redditclone.repository.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{
    public final CommentRepository commentRepository;

    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.getAllComments();
    }

    @Override
    public void addComment(Comment comment) {
        commentRepository.addComment(comment);
    }

    @Override
    public Comment findCommentById(Integer id) {
        return commentRepository.getCommentById(id);
    }

    @Override
    public void updateComment(Comment comment) {
        commentRepository.updateComment(comment);
    }
}
