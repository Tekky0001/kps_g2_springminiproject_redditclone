package com.example.kps_g2.redditclone.service;

import com.example.kps_g2.redditclone.model.SubReddit;
import com.example.kps_g2.redditclone.repository.SubredditRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class SubredditServiceImpl implements SubredditService{
    private final SubredditRepository subredditRepository;

    public SubredditServiceImpl(SubredditRepository subredditRepository) {
        this.subredditRepository = subredditRepository;
    }

    @Override
    public List<SubReddit> getAllSubReddits() {
        return subredditRepository.getAllSubReddits();
    }

    @Override
    public SubReddit getSubRedditById(Integer id) {
        return subredditRepository.getSubRedditById(id);
    }

    @Override
    public void insertSubReddit(SubReddit subReddit) {
        subredditRepository.insertSubReddit(subReddit);
    }

    @Override
    public void updateSubReddit(SubReddit subReddit) {
        subredditRepository.updateSubReddit(subReddit);
    }

    @Override
    public void deleteSubRedditById(Integer id) {
        subredditRepository.deleteSubRedditById(id);
    }
}
