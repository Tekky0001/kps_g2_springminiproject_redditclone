package com.example.kps_g2.redditclone.service;

import com.example.kps_g2.redditclone.model.Post;
import com.example.kps_g2.redditclone.repository.PostRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService{

    private final PostRepository postRepository;


    public PostServiceImpl(PostRepository postRepository )
    {
        this.postRepository = postRepository;
    }


    @Override
    public List<Post> getAllPosts() {
        return this.postRepository.getAllPostsFromDB();
    }


    @Override
    public Post findPostById(Integer id) {

        return postRepository.getPostById(id);
    }

    @Override
    public void updatePost(Post post) {
        postRepository.updatePost(post);
    }

    @Override
    public List<Post> getAllPostsBySubRedditId(int id) {
        return postRepository.getAllPostsBySubredditId(id);
    }

    @Override
    public void insertPost(Post post) {
        postRepository.insertPost(post);
    }

    @Override
    public void deletePostById(Integer id) {
        postRepository.deletePostById(id);
    }


}
