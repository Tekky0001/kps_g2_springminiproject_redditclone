package com.example.kps_g2.redditclone.service;

import com.example.kps_g2.redditclone.model.SubReddit;

import java.util.List;

public interface SubredditService {
    List<SubReddit> getAllSubReddits();

    SubReddit getSubRedditById(Integer id);

    void insertSubReddit(SubReddit subReddit);

    void updateSubReddit(SubReddit subReddit);

    void deleteSubRedditById(Integer id);
}
