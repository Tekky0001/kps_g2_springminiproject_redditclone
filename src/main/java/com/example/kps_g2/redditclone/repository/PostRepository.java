package com.example.kps_g2.redditclone.repository;

import com.example.kps_g2.redditclone.model.Post;
import org.apache.ibatis.annotations.*;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;


import java.util.List;

@Mapper
public interface PostRepository {
    @Select("SELECT * FROM posts")
    @Result(property="postId", column="id")
    @Result(property="postTitle", column="post_title")
    @Result(property="description", column="post_description")
    @Result(property="imageUrl", column="image_url")
    @Result(property="subReddit", column="subreddit_id",
            one = @One(select="com.example.kps_g2.redditclone.repository.SubredditRepository.getSubRedditById"
            ))
    @Result(property="createdDate", column="created_date")
    @Result(property="vote", column="vote")
    @Result(property = "comments", column = "id",
            many = @Many(
                    select = "com.example.kps_g2.redditclone.repository.CommentRepository.getCommentsByPostId"
            ))
    List<Post> getAllPostsFromDB();


    @Select("SELECT * FROM posts WHERE id=#{id}")
    @Result(property="postId", column="id")
    @Result(property="postTitle", column="post_title")
    @Result(property="description", column="post_description")
    @Result(property="imageUrl", column="image_url")
    @Result(property="subReddit", column="subreddit_id",
            one = @One(select="com.example.kps_g2.redditclone.repository.SubredditRepository.getSubRedditById"
            ))
    @Result(property="createdDate", column="created_date")
    @Result(property="vote", column="vote")
    @Result(property = "comments", column = "id",
            many = @Many(
                    select = "com.example.kps_g2.redditclone.repository.CommentRepository.getCommentsByPostId"
            ))
    Post getPostById(Integer id);



    @Results(value={
            @Result(property="postId", column="id"),
            @Result(property="postTitle", column="post_title"),
            @Result(property="description", column="post_description"),
            @Result(property="imageUrl", column="image_url"),
            @Result(property="subReddit", column="subreddit_id",
                    one = @One(select="com.example.kps_g2.redditclone.repository.SubredditRepository.getSubRedditById"
                    )),
            @Result(property="createdDate", column="created_date"),
            @Result(property="vote", column="vote"),
    })
    @Update("UPDATE posts SET post_title = #{post.postTitle}, post_description = #{post.description}, image_url = #{post.imageUrl}, subreddit_id = #{post.subReddit.id}, created_date = #{post.createdDate}, vote = #{post.vote} WHERE id = #{post.postId}")
    void updatePost(@Param("post") Post post);

    @Select("SELECT * FROM posts WHERE subreddit_id = #{id}")
    @Result(property="postId", column="id")
    @Result(property="postTitle", column="post_title")
    @Result(property="description", column="post_description")
    @Result(property="imageUrl", column="image_url")
    @Result(property="subReddit", column="subreddit_id",
            one = @One(select="com.example.kps_g2.redditclone.repository.SubredditRepository.getSubRedditById"
            ))
    @Result(property="createdDate", column="created_date")
    @Result(property="vote", column="vote")
    @Result(property = "comments", column = "id",
            many = @Many(
                    select = "com.example.kps_g2.redditclone.repository.CommentRepository.getCommentsByPostId"
            ))
    List<Post> getAllPostsBySubredditId(int id);

    @Insert("INSERT INTO posts (post_title, post_description, image_url, subreddit_id, created_date, vote) VALUES (#{post.postTitle},#{post.description}," +
            "#{post.imageUrl}, #{post.subReddit.id}, #{post.createdDate}, #{post.vote})")
    void insertPost(@Param("post") Post post);

    @Delete("DELETE FROM posts WHERE id = #{id}")
    void deletePostById(Integer id);
}
