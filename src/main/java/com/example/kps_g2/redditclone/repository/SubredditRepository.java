package com.example.kps_g2.redditclone.repository;

import com.example.kps_g2.redditclone.model.SubReddit;
import org.apache.ibatis.annotations.*;
import org.hibernate.validator.constraints.pl.REGON;

import java.util.List;


@Mapper
public interface SubredditRepository {
    @Select("SELECT * FROM subreddits WHERE id = #{id}")
    @Result(property = "subRedditTitle", column = "sub_red_title")
    SubReddit getSubRedditById(Integer id);


    @Select("SELECT * FROM subreddits")
    @Result(property = "subRedditTitle", column = "sub_red_title")
    List<SubReddit> getAllSubReddits();


    @Insert("INSERT INTO subreddits (sub_red_title) VALUES (#{subreddit.subRedditTitle})")
    @Result(property = "subRedditTitle", column = "sub_red_title")
    void insertSubReddit(@Param("subreddit") SubReddit subReddit);

    @Update("UPDATE subreddits SET sub_red_title = #{subreddit.subRedditTitle} WHERE id = #{subreddit.id}")
    @Result(property = "subRedditTitle", column = "sub_red_title")
    void updateSubReddit(@Param("subreddit") SubReddit subReddit);


    @Delete("DELETE FROM subreddits WHERE id = #{id}")
    void deleteSubRedditById(Integer id);
}
