package com.example.kps_g2.redditclone.repository;

import com.example.kps_g2.redditclone.model.Comment;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;


import java.util.List;

@Mapper
public interface CommentRepository {
    @Select("SELECT * FROM comments WHERE post_id = #{postId}")
    @Result(property = "postId", column="post_id")
    @Result(property = "createdDate", column = "created_date")
    List<Comment> getCommentsByPostId(Integer postId);

    @Select("SELECT * FROM comments")
    @Result(property = "postId", column="post_id")
    @Result(property = "createdDate", column = "created_date")
    List<Comment> getAllComments();

    @Results(value = {
            @Result(property = "postId", column = "post_id"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "description", column = "description"),
            @Result(property = "vote", column = "vote")
    })

    @Insert("INSERT INTO comments ( description, created_date, vote, post_id) VALUES (#{description}, #{createdDate}, #{vote},#{postId})")
    void addComment(Comment comment);

    @Select("SELECT * FROM comments WHERE id = #{id}")
    @Result(property = "postId", column="post_id")
    @Result(property = "createdDate", column = "created_date")
    Comment getCommentById(Integer id);

    @Update("UPDATE comments SET vote = #{comment.vote} WHERE id = #{comment.id}")
    void updateComment(@Param("comment") Comment comment);

}
