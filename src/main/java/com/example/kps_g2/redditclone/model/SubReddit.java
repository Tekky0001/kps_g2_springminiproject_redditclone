package com.example.kps_g2.redditclone.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubReddit {
    private Integer id;
    private String subRedditTitle;
}
