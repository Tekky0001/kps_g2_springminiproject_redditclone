package com.example.kps_g2.redditclone.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private Integer postId;
    private String postTitle;
    private String description;
    private String imageUrl;
    private SubReddit subReddit;
    private LocalDateTime createdDate;
    private int vote;
    private List<Comment> comments;
}
