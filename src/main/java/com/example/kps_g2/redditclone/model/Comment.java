package com.example.kps_g2.redditclone.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    private Integer id;
    private String description;
    private Integer postId;
    private int vote;
    private LocalDateTime createdDate;
}
