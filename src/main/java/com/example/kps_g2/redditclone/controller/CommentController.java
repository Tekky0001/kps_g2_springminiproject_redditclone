package com.example.kps_g2.redditclone.controller;

import com.example.kps_g2.redditclone.model.Comment;
import com.example.kps_g2.redditclone.model.Post;
import com.example.kps_g2.redditclone.service.CommentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;

@Controller
@RequestMapping("comment")
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/add/{id}")
    public ModelAndView postComment(@PathVariable("id") int id,
                                    @ModelAttribute("comment") Comment comment){
        ModelAndView mv = new ModelAndView();
        comment.setDescription(comment.getDescription());
        comment.setPostId(id);
        comment.setVote(0);
        comment.setCreatedDate(LocalDateTime.now());
        commentService.addComment(comment);
        System.out.println(comment);
        mv.addObject("comment",comment);
        mv.setViewName("redirect:/posts/read-post/"+id);
        return mv;
    }
    @GetMapping("/upvote/comment/{id}")
    @ResponseBody
    public ModelAndView upvoteComment(@RequestParam String vote,
                                      @PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        int numVote = Integer.parseInt(vote);
        if(numVote == 1){
            numVote = 0;
        }else if(numVote ==0){
            numVote = 1;
        }else if(numVote == -1){
            numVote = 1;
        }
        Comment comment = commentService.findCommentById(id);
        comment.setVote(numVote);
        commentService.updateComment(comment);
        System.out.println("vote: "+numVote);
        mv.setViewName("redirect:/posts/read-post/"+comment.getPostId());
        return mv;
    }

    @GetMapping("/downvote/comment/{id}")
    @ResponseBody
    public ModelAndView downvoteComment(@RequestParam String vote,
                                      @PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        int numVote = Integer.parseInt(vote);
        if(numVote == 1){
            numVote = -1;
        }else if(numVote ==0){
            numVote = -1;
        }else if(numVote == -1){
            numVote = 0;
        }
        Comment comment = commentService.findCommentById(id);
        comment.setVote(numVote);
        commentService.updateComment(comment);
        System.out.println("vote: "+numVote);
        mv.setViewName("redirect:/posts/read-post/"+comment.getPostId());
        return mv;
    }
}
