package com.example.kps_g2.redditclone.controller;

import com.example.kps_g2.redditclone.model.Comment;
import com.example.kps_g2.redditclone.model.Post;
import com.example.kps_g2.redditclone.model.SubredditPOJO;
import com.example.kps_g2.redditclone.service.CommentService;
import com.example.kps_g2.redditclone.service.FileStorageService;
import com.example.kps_g2.redditclone.service.PostService;
import com.example.kps_g2.redditclone.service.SubredditService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/posts")
public class PostController {
    private final PostService postService;
    private final SubredditService subredditService;
    private final CommentService commentService;
    private final FileStorageService fileStorageService;

    public PostController(PostService postService, SubredditService subredditService, CommentService commentService, FileStorageService fileStorageService) {
        this.postService = postService;
        this.subredditService = subredditService;
        this.commentService = commentService;
        this.fileStorageService = fileStorageService;
    }


    @GetMapping("")
    public ModelAndView homePage(){
        ModelAndView mv = new ModelAndView();
        System.out.println(postService.getAllPosts());
        SubredditPOJO subredditPOJO = new SubredditPOJO();
        subredditPOJO.setSubreddits(subredditService.getAllSubReddits());
        mv.setViewName("index");
        mv.addObject("subreddits", subredditPOJO);
        List<Post> list = postService.getAllPosts().stream()
                        .sorted((o1,o2)->o1.getPostId().compareTo(o2.getPostId()))
                        .collect(Collectors.toList());
        mv.addObject("posts",list);

        return mv;
    }
    @GetMapping("/filter")
    @ResponseBody
    public ModelAndView indexPage(@RequestParam String subreddit){
        ModelAndView mv = new ModelAndView();
        System.out.println(postService.getAllPosts());
        System.out.println("param: "+subreddit);
        SubredditPOJO subredditPOJO = new SubredditPOJO();
        subredditPOJO.setSubreddits(subredditService.getAllSubReddits());
        mv.setViewName("index");
        mv.addObject("subreddits", subredditPOJO);
        mv.addObject("posts", postService.getAllPostsBySubRedditId(Integer.parseInt(subreddit)));
        return mv;
    }

    @GetMapping("/read-post/{id}")
    public ModelAndView viewPost(@PathVariable Integer id,
                                 @ModelAttribute("comment") Comment comment){
        ModelAndView mv = new ModelAndView();
        Post post = postService.findPostById(id);
        mv.setViewName("read-post");
        //System.out.println("post = "+ post);
        post.setComments(post.getComments().stream()
                        .sorted(((o1, o2) -> o1.getId().compareTo(o2.getId())))
                        .collect(Collectors.toList()));
        mv.addObject("post",post);
        mv.addObject("comment",comment);

        return mv;
    }
    @GetMapping("/read-post/{id}/discard")
    public ModelAndView discardComment(@PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/posts/read-post/"+id);
        return mv;
    }
    @GetMapping("/edit-post/{id}")
    public ModelAndView editPost(@PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        Post post = postService.findPostById(id);
        SubredditPOJO subredditPOJO = new SubredditPOJO();
        subredditPOJO.setSubreddits(subredditService.getAllSubReddits());

        mv.addObject("post", post);
        mv.addObject("subreddits", subredditPOJO);
        mv.setViewName("edit-post");
        return mv;
    }

    @PostMapping("/update-post/{id}")
    public ModelAndView updatePost(@ModelAttribute("post") Post post,
                                   @PathVariable("id") int id,
                                   @RequestParam("file") MultipartFile file){
        ModelAndView mv = new ModelAndView();
        Post oldPost = postService.findPostById(id);
        // try to save file
        String imageUrl = oldPost.getImageUrl();
        if(!file.isEmpty()){
            try {
                imageUrl = "http://localhost:8080/images/" + fileStorageService.saveFile(file);
                System.out.println("File URL: " + imageUrl);

            } catch (IOException ex) {

                System.out.println("File Exception : " + ex.getMessage());
            }
        }
        post.setImageUrl(imageUrl);
        post.setComments(oldPost.getComments());
        post.setCreatedDate(LocalDateTime.now());
        post.setSubReddit(subredditService.getSubRedditById(post.getSubReddit().getId()));
        System.out.println("post: "+post);
        postService.updatePost(post);

        mv.setViewName("redirect:/posts");
        return mv;
    }
    @GetMapping("/edit-post/discard/{id}")
    public ModelAndView discardPost(@PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/posts/edit-post/"+id);
        return mv;
    }

    @GetMapping("/upvote/post/{id}")
    @ResponseBody
    public ModelAndView upvotePost(@RequestParam String vote,
                                   @RequestParam String page,
                                   @PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        int numVote = Integer.parseInt(vote);
        if(numVote == 1){
            numVote = 0;
        }else if(numVote ==0){
            numVote = 1;
        }else if(numVote == -1){
            numVote = 1;
        }
        Post post = postService.findPostById(id);
        post.setVote(numVote);
        postService.updatePost(post);
        System.out.println("vote: "+numVote);
        if(page.equals("index")) {
            mv.setViewName("redirect:/posts");
        }
        else if(page.equals("readpost")) {
            mv.setViewName("redirect:/posts/read-post/"+id);
        }
        return mv;
    }
    @GetMapping("/downvote/post/{id}")
    @ResponseBody
    public ModelAndView downvotePost(@RequestParam String vote,
                                     @RequestParam String page,
                                     @PathVariable Integer id){
        ModelAndView mv = new ModelAndView();
        int numVote = Integer.parseInt(vote);
        if(numVote == 1){
            numVote = -1;
        }else if(numVote ==0){
            numVote = -1;
        }else if(numVote == -1){
            numVote = 0;
        }
        Post post = postService.findPostById(id);
        post.setVote(numVote);
        postService.updatePost(post);
        System.out.println("vote: "+numVote);
        if(page.equals("index")) {
            mv.setViewName("redirect:/posts");
        }
        else if(page.equals("readpost")) {
            mv.setViewName("redirect:/posts/read-post/"+id);
        }
        return mv;
    }

    @GetMapping("/create-post")
    public ModelAndView createPostForm(){
        ModelAndView mvs = new ModelAndView();
        mvs.setViewName("post");
        Post post = new Post();
        mvs.addObject("post",post);
        SubredditPOJO subredditPOJO = new SubredditPOJO();
        subredditPOJO.setSubreddits(subredditService.getAllSubReddits());
        mvs.addObject("subredditlistObj", subredditPOJO);
        return mvs;
    }
    @PostMapping("/create-a-post")
    public ModelAndView createNewPost(@ModelAttribute("post") Post post,
                                      @RequestParam("file") MultipartFile file){
        ModelAndView mv = new ModelAndView();
        // try to save file
        String imageUrl = null;
        if(!file.isEmpty()){
            try {
                imageUrl = "http://localhost:8080/images/" + fileStorageService.saveFile(file);
                System.out.println("File URL: " + imageUrl);

            } catch (IOException ex) {

                System.out.println("File Exception : " + ex.getMessage());
            }
        }
        post.setImageUrl(imageUrl);
        post.setCreatedDate(LocalDateTime.now());
        post.setSubReddit(subredditService.getSubRedditById(post.getSubReddit().getId()));
        postService.insertPost(post);
        mv.setViewName("redirect:/posts");
        return mv;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deletePostReddit(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();
        postService.deletePostById(id);
        mv.setViewName("redirect:/posts");
        return mv;
    }



}
