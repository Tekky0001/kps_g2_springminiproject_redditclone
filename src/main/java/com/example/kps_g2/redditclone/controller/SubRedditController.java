package com.example.kps_g2.redditclone.controller;


import com.example.kps_g2.redditclone.model.SubReddit;
import com.example.kps_g2.redditclone.model.SubredditPOJO;
import com.example.kps_g2.redditclone.service.SubredditService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.rmi.MarshalledObject;
import java.util.List;

@Controller
@RequestMapping("/subreddit")
public class SubRedditController {

    private final SubredditService subredditService;

    public SubRedditController(SubredditService subredditService) {
        this.subredditService = subredditService;
    }

    @GetMapping("/subredditform")
    public ModelAndView displaySubredditForm() {
        ModelAndView mv = new ModelAndView();
        SubReddit subReddit = new SubReddit();
        mv.setViewName("create-subreddit");
        mv.addObject("subReddit", subReddit);
        return mv;
    }

    @PostMapping("/create-subreddit")
    public ModelAndView createSubreddit(@ModelAttribute ("subReddit")SubReddit subReddit) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/posts");
        mv.addObject("subReddit", subReddit);
        System.out.println(subReddit);
        subredditService.insertSubReddit(subReddit);
        return mv;
    }

    @GetMapping("/edit-subredditform/{id}")
    public ModelAndView editSubredditForm(@PathVariable Integer id,
                                          @RequestParam String page) {
        ModelAndView mv = new ModelAndView();
        SubReddit subReddit = subReddit = subredditService.getSubRedditById(id);;
        if(page.equals("discardreddit")){
            subReddit = new SubReddit();
        }


        mv.addObject("subReddit", subReddit);
        mv.setViewName("edit-subreddit");
        return mv;
    }

    @PostMapping("/update-subreddit/{id}")
    public ModelAndView updateSubreddit(@ModelAttribute ("subReddit")SubReddit subReddit, @PathVariable Integer id)
    {
        ModelAndView mv = new ModelAndView();
        mv.addObject("subReddit", subReddit);
        System.out.println(subReddit);
        subReddit.setId(id);
        subredditService.updateSubReddit(subReddit);
        mv.setViewName("redirect:/posts");
        return mv;
    }

    @GetMapping("/discard-subreddit")
    public ModelAndView discardSubReddit() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/subreddit/subredditform");
        return mv;
    }

    @GetMapping("/discard-edit/{id}")
    public ModelAndView discardEdit(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/subreddit/edit-subredditform/"+id+"?page=discardreddit");
        return mv;
    }

    @GetMapping("/list-subreddits")
    public ModelAndView displaySubReddits() {
        ModelAndView mv = new ModelAndView();
        List<SubReddit> subReddits = subredditService.getAllSubReddits();
        SubredditPOJO subredditPOJO = new SubredditPOJO();
        subredditPOJO.setSubreddits(subReddits);
        mv.setViewName("list-Subreddit");
        mv.addObject("subReddits", subredditPOJO);
        return mv;
    }

    @GetMapping("/delete-subreddit/{id}")
    public ModelAndView deleteSubreddit(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();
        subredditService.deleteSubRedditById(id);
        mv.setViewName("redirect:/subreddit/list-subreddits");
        return mv;
    }

}
